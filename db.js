var Sequelize = require('sequelize');
var Faker = require('faker');
var _ = require('lodash');


if (process.env.DATABASE_URL) {
    // the application is executed on Heroku ... use the postgres database
    var Conn = new Sequelize(process.env.DATABASE_URL, {
      dialect:  'postgres',
      protocol: 'postgres',
      logging:  true //false
    });
} else {
    var Conn = new Sequelize(
        'relay',
        'jeffrey',
        'postgres',
        {
          dialect: 'postgres',
          host: 'localhost'
        }
    );
}

var Person = Conn.define('person', {
  firstName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  lastName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  email: {
    type: Sequelize.STRING,
    validate: {
      isEmail: true
    }
  }
});

var Post = Conn.define('post', {
  title: {
    type: Sequelize.STRING,
    allowNull: false
  },
  content: {
    type: Sequelize.STRING,
    allowNull: false
  }
});

// Relations
Person.hasMany(Post);
Post.belongsTo(Person);

Conn.sync({ force: true }).then(()=> {
  _.times(10, ()=> {
    return Person.create({
      firstName: Faker.name.firstName(),
      lastName: Faker.name.lastName(),
      email: Faker.internet.email()
    }).then(person => {
      return person.createPost({
        title: `Sample post by ${person.firstName}`,
        content: 'here is some content'
      });
    });
  });
});

module.exports = Conn;
