var {   graphql,
        GraphQLObjectType,
        GraphQLInt,
        GraphQLString,
        GraphQLList,
        GraphQLSchema,
        GraphQLNonNull,
    } = require('graphql');
var Db  = require('./db');

var Person = new GraphQLObjectType({
    name: 'Person',
    description: 'This represents a person',
    fields: ()=> {
        return {
            id: {
                type: GraphQLInt,
                resolve(person) {
                    return person.id;
                }
            },
            firstName: {
                type: GraphQLString,
                resolve(person) {
                    return person.firstName;
                }
            },
            lastName: {
                type: GraphQLString,
                resolve(person) {
                    return person.lastName;
                }
            },
            email: {
                type: GraphQLString,
                resolve(person) {
                    return person.email;
                }
            },
            post: {
                type: new GraphQLList(Post),
                resolve(person) {
                    return person.getPosts();
                }
            }
        }
    }
})

var Post = new GraphQLObjectType({
    name: 'Post',
    description: 'Stuff written by Persons',
    fields: () => {
        return {
            id: {
                type: GraphQLInt,
                resolve(post) {
                    return post.id;
                }
            },
            title: {
                type: GraphQLString,
                resolve(post) {
                    return post.title;
                }
            },
            content: {
                type: GraphQLString,
                resole(post) {
                    return post.content;
                }
            },
            person: {
                type: Person,
                resolve(post) {
                    return post.getPerson();
                }
            }
        };
    }
})

var Query = new GraphQLObjectType({
    name: 'Query',
    description: 'Query',
    fields: () => {
        return {
            people: {
                type: new GraphQLList(Person),
                args: {
                    id: {
                        type: GraphQLInt
                    },
                    email: {
                        type: GraphQLString
                    }
                },
                resolve(root, args) {
                    return Db.models.person.findAll({where: args});
                }
            },
            post: {
                type: new GraphQLList(Post),
                resolve(root, args) {
                    return Db.models.post.findAll({where: args});
                }
            },
        };
    }
})

var Mutation = new GraphQLObjectType({
    name: 'mutation',
    desciption: 'functions to create data',
    fields: () => {
        return {
            addPerson: {
                type: Person,
                args: {
                    firstName: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    lastName: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    email: {
                        type: new GraphQLNonNull(GraphQLString)
                    }
                },
                resolve(root, args) {
                    return Db.models.person.create({
                        firstName: args.firstName,
                        lastName: args.lastName,
                        email: args.email.toLowerCase()
                    });
                }
            }
        };
    }
});

var Schema = new GraphQLSchema({
    query: Query,
    mutation: Mutation
});

module.exports = Schema;
