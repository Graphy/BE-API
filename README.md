For a live demo: https://fathomless-hamlet-67198.herokuapp.com/graphql

A query you can use to fetch all people is:

```
{
  people {
    id,
    firstName,
    lastName,
    email
  }
}
```

A query to fetch all posts is:

```
{
  post {
    id,
    title,
    content
  }
}
```

Combined to fetch all data based on posts:

```
{
  post {
    id,
    title,
    content
    person{
      firstName,
      lastName,
      id,
      email
    }
  }
}
```

Combined to fetch all data based on people:

```
{
  people{
    id,
    firstName,
    lastName,
    email
    post {
      id,
      title,
      content
    }
  }
}
```

Add person to DB by mutation:

```
mutation {
  addPerson (
    firstName: "yolo"
    lastName: "oloy"
    email: "yolo@yo.lo"
  ) {
    firstName
    lastName
    email
  }
}
```