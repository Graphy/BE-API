var Express = require('express');
var GraphHTTP = require('express-graphql');
var Schema = require('./schema');

// CONFIG

var APP_PORT = process.env.PORT || 8080;

var app = Express();

app.use("/graphql", function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    if (req.method === 'OPTIONS') {
      res.sendStatus(200);
    } else {
      next();
    }
});

app.use('/graphql', GraphHTTP({
    schema: Schema,
    pretty: true,
    graphiql: true
}));

app.listen(APP_PORT, () => {
    console.log(`Yay mate! Server is running on port ${APP_PORT}`)
})
